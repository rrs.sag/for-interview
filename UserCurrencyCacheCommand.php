<?php

use \app\report\filters\TransactionTypes;
use \app\report\filters\TransactionStatuses;
use \app\report\filters\CachedData;
use \app\report\filters\CachedDataForUser;
use \app\report\filters\PaymentTypes;
use \app\report\filters\Currencies;
use \app\report\filters\PeriodFilterDataProvider;

final class UserCurrencyCacheCommand extends ConsoleCommand
{
    const
        NEW_CACHE = true,
        CHECK_INTERVAL = 600;

    private  $period = CachedData::KEY_FOR_12_MONTH;


    public function actionIndex()
    {
        $this->runExclusive(sys_get_temp_dir() . '/' . __CLASS__ . '.lock');

        if ($this->checkCacheIsUpToDate()) {
            return;
        }

        if (CachedData::isNotExpiredOrEmptyCache()) {
            $this->period = CachedData::KEY_FOR_2_MONTH;
        }


        (new CachedDataForUser(self::NEW_CACHE,$this->period,CachedData::TRANSACTION_TYPES,new PeriodFilterDataProvider(new TransactionTypes(),$this->is12MonthPeriod())))->data();
        (new CachedDataForUser(self::NEW_CACHE,$this->period,CachedData::TRANSACTION_STATUSES, new PeriodFilterDataProvider(new TransactionStatuses(),$this->is12MonthPeriod())))->data();
        (new CachedDataForUser(self::NEW_CACHE,$this->period,CachedData::CURRENCIES, new PeriodFilterDataProvider(new Currencies(),$this->is12MonthPeriod())))->data();
        (new CachedDataForUser(self::NEW_CACHE,$this->period,CachedData::PAYMENT_TYPES, new PeriodFilterDataProvider(new PaymentTypes(),$this->is12MonthPeriod())))->data();
        

        Yii::app()->cache->set(__CLASS__, time());

        if ($this->is12MonthPeriod()) {
            Yii::app()->cache->set(CachedData::CHECK_CACHE_KEY, true);
        }

    }

    public function actionClear()
    {
        Yii::app()->cache->delete(CachedData::CHECK_CACHE_KEY);
        Yii::app()->cache->delete(__CLASS__);
    }

    private function checkCacheIsUpToDate()
    {
        $lastUpdateTimestamp = Yii::app()->cache->get(__CLASS__);

        if (empty($lastUpdateTimestamp)) {
            return false;
        }

        $diff = time() - $lastUpdateTimestamp;

        return $diff <= self::CHECK_INTERVAL;
    }

    public function is12MonthPeriod()
    {
        return $this->period == CachedData::KEY_FOR_12_MONTH;
    }
}
