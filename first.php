<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
include $GLOBALS;
// Выводит документы в то метсо где в деатльном описании размещенно #DOCUMENT_*#
if(!empty($mass['PROPERTIES']['DOCUMENTS']['VALUE'])){
	foreach($mass['PROPERTIES']['DOCUMENTS']['VALUE'] as $key=>$doc){
		$str = '';
		$path = CFile::GetPath($doc);
		$desc_with_ext = substr($path,strrpos($path,'/')+1);
		$ext = substr($path,strrpos($path,'.')+1);
		$desc = substr($desc_with_ext,0,strrpos($desc_with_ext,'.'));
		
		$str = '<a class="loadItem" href="'.$path.'">
			<svg class="loadItem_icon symbol symbol-'.$ext.' symbol-info" role="img">
			    <use xlink:href="/img/symbols/sprite.svg#'.$ext.'"/>
			</svg>
			<div class="loadItem_content">
				<span class="loadItem_info"></span>
				<div class="loadItem_desc">'.$desc.'</div>
			</div>
			</a>';
		$mass['DETAIL_TEXT'] = str_replace("#DOCUMENT_".($key+1)."#", $str, $mass['DETAIL_TEXT']);
	}		
}

// Выводит фотогалерею в то метсо где в деатльном описании размещенно #PHOTO_*#
if(!empty($mass['PROPERTIES']['PHOTO_ALBUM']['VALUE'])){
	foreach($mass['PROPERTIES']['PHOTO_ALBUM']['VALUE'] as $key=>$pht){
		ob_start();
		$GLOBALS['num'] = $pht;
		include('photo.php');
		$photos = ob_get_contents();
		ob_end_clean();
		$mass['DETAIL_TEXT'] = str_replace("#PHOTO_".($key+1)."#", $photos, $mass['DETAIL_TEXT']);
	}
}


// Выводит видеоз в то метсо где в деатльном описании размещенно #VIDEO_*#
if(!empty($mass['PROPERTIES']['VIDEO_LINK']['VALUE'])){
	foreach($mass['PROPERTIES']['VIDEO_LINK']['VALUE'] as $key=>$link){
		$video_link = '<div class="video">
				<iframe width="640" height="390" frameborder="0" src="'.$link.'" allowfullscreen=""></iframe>
			</div>';

		$mass['DETAIL_TEXT'] = str_replace("#VIDEO_".($key+1)."#", $video_link, $mass['DETAIL_TEXT']);
	}
}
?>

