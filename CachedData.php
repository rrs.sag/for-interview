<?php

namespace app\report\filters;

class CachedData implements IData
{
    const
        TRANSACTION_TYPES = 'getTransactionTypes',
        TRANSACTION_STATUSES = 'getTransactionStatuses',
        CURRENCIES = 'getUserCurrencies',
        PAYMENT_TYPES = 'getUserPaymentTypes',
        KEY_FOR_2_MONTH = '2month',
        KEY_FOR_12_MONTH = '12month',
        CHECK_CACHE_KEY = 'userFilterCache',
        KEY_DELIMITER = '_'
    ;

    private $newCache = false;

    private $dataKey;

    private $ttl = 3600;

    /**
     * @var IFilterData
     */
    private $origin;


    /**
     * CachedFilter constructor.
     * @param bool $renewCache
     * @param string $dataKey
     * @param int $ttl
     * @param IData $origin
     */
    public function __construct($newCache, $dataKey, IData $origin, $ttl = 3600)
    {
        $this->newCache = $newCache;
        $this->dataKey = $dataKey;
        $this->ttl = $ttl;
        $this->origin = $origin;
    }

    public function data()
    {
        $data = [];

        if (!$this->renewCache) {
            $data = $this->getData();
        }

        if (empty($data)) {

            $data = $this->origin->data();

            if (!empty($data)) {
                $this->setData($data);
                $data = json_encode($data);
            }
        }

        return
            empty($data)
                ? []
                : json_decode($data, true)
            ;
    }

    public function getData()
    {
        $data = \Yii::app()->cache->get($this->dataKey);

        return
            empty($data)
                ? []
                : json_decode($data, true)
            ;
    }

    /**
     * @param array $data
     */
    public function setData(array $data)
    {
        $data = json_encode($data);

        \Yii::app()->cache->set($this->dataKey, $data, $this->ttl);

        return $this;
    }

    public static function isNotExpiredOrEmptyCache()
    {
        return \Yii::app()->cache->get(self::CHECK_CACHE_KEY);
    }

    /**
     * @param string $dataKey
     */
    public function changeDataKey($dataKey)
    {
        $this->dataKey = $dataKey;
    }
}