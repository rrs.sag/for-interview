<?php

namespace app\report\filters;


class CachedDataForUser implements IData
{
    private $newCache = false;

    private $baseKey;

    private $periodKey;

    private $origin;

    /**
     * userCachedData constructor.
     * @param bool $newCache
     * @param string $periodKey
     * @param string $baseKey
     * @param IFilterData $origin
     */

    public function __construct($newCache, $periodKey, $baseKey, IData $origin)
    {
        $this->newCache = $newCache;
        $this->periodKey = $periodKey;
        $this->baseKey = $baseKey;
        $this->origin = $origin;
    }

    public function data()
    {
        $dataForPeriodObj = new CachedData(
            $this->newCache,
            $this->baseKey. CachedData::KEY_DELIMITER .$this->periodKey,
            $this->origin
        );

        $dataForPeriod = $dataForPeriodObj->data();

        $usersIDs = array_keys($dataForPeriod);

        foreach($usersIDs as $userID){
            $data = array($userID=>$dataForPeriod[$userID]);

            $dataForPeriodObj->changeDataKey($this->baseKey. CachedData::KEY_DELIMITER . $userID . CachedData::KEY_DELIMITER .$this->periodKey);

            $dataForPeriodObj->setData($data);
        }

        return $dataForPeriod;
    }
}