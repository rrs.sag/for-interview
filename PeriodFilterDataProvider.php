<?php

namespace app\report\filters;

class PeriodFilterDataProvider implements IData
{
    /**
     * @var bool
     */
    private $is12MonthData = false;

    /**
     * @var IPeriodFilterData
     */
    private $dataProvider;

    public function __construct(IPeriodFilterData $dataProvider, $is12MonthData = false)
    {
        $this->is12MonthData = $is12MonthData;
        $this->dataProvider = $dataProvider;
    }

    public function data()
    {
        return
            !$this->is12MonthData
                ? $this->dataProvider->getDataForPeriod(self::getPeriodFor2LastMonth())
                : $this->dataProvider->get12MonthLastData();
    }

    public static function getPeriodFor2LastMonth()
    {
        return date('Y-m-d\\TH:i:s+00:00', strtotime(date('Y-m')." -1 month"));
    }
}