<?php
/**
 * Created by PhpStorm.
 * User: Ruslan
 * Date: 19.05.2018
 * Time: 11:18
 */

function quickSort(&$array, $left = null, $right = null) {

    if (is_null($left) || is_null($right)) {
        $left = 0;
        $right = count($array) - 1;
    }

    $left_current = $left;
    $right_current = $right;

    $middle = $array[(int)($left_current + $right_current) / 2];

    while ($left_current <= $right_current) {

        while ($array[$left_current] < $middle) {
            $left_current++;
        }

        while ($array[$right_current] > $middle) {
            $right_current--;
        }

        if ($left_current <= $right_current) {

            list($array[$right_current], $array[$left_current]) = array($array[$left_current], $array[$right_current]);

            $left_current++;
            $right_current--;
        }
    }

    if ($left_current < $right) {
        quickSort($array, $left_current, $right);
    }

    if ($right_current > $left) {
        quickSort($array, $left, $right_current);
    }
}

function binarySearch($mass,$q){
    $left = 0;
    $right = count($mass)-1;
    $search = null;
    while($left < $right) {
        $middle = round(($left+$right)/2);

        if($q == $mass[$left] ){
            $search = $left;
            break;
        }

        if($q == $mass[$right] ){
            $search = $right;
            break;
        }

        if($q == $mass[$middle] ){
            $search = $middle;
            break;
        }

        if($q == $mass[$middle] ){
            $search = $middle;
            break;
        }

        if($q < $mass[$middle]){
            $right = $middle - 1;
        }else{
            $left = $middle + 1;
        }
    }
    return $search;
}


function intersect($a,$b){
    $intersect = array();
    quickSort($b);
    foreach($a as $key=>$value){
        if(!is_null(binarySearch($b,$value))){
            $intersect[] = $value;
        }
    }
    return $intersect;
}


$a = array (24,1,53,11,78,32,12);
$b = array (595,24,56,90,12,78,22);

$answer = intersect($a,$b);
print_r($answer);