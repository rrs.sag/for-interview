<?php

namespace app\report\filters;

class CachedDataForFilters implements IData
{
    /**
     * @var IData
     */
    private $dataKey;

    private $origin;


    /**
     * CachedDataForFilters constructor.
     * @param $dataKey
     * @param IData $origin
     */
    public function __construct($dataKey, IPeriodData $origin)
    {
        $this->dataKey = $dataKey;
        $this->origin = $origin;
    }

    public function data()
    {
        $for12MonthData  =
            (new CachedData(
                false,
                (string)$this->dataKey. CachedData::KEY_DELIMITER .CachedData::KEY_FOR_12_MONTH,
                $this->origin
            ))->getData();

        $for2MonthDataObj = new CachedData(
            false,
            (string)$this->dataKey. CachedData::KEY_DELIMITER. CachedData::KEY_FOR_2_MONTH,
            $this->origin
        );

        $for2MonthData = $for2MonthDataObj->getData();

        if (empty($for2MonthData)){
            $for2MonthData = $this->origin->getDataForPeriod(PeriodFilterDataProvider::getPeriodFor2LastMonth());

            $for2MonthDataObj->setData($for2MonthData);
        }

        return $this->mergeCacheData($for12MonthData, $for2MonthData);
    }

    private function mergeCacheData(array $for12MonthData,array $for2MonthData)
    {
        $existCacheData = array_intersect_key($for2MonthData,$for12MonthData);

        $newCacheData = array_diff_key($for2MonthData,$existCacheData);

        $cacheData = $for12MonthData + $newCacheData;

        $cacheData = array_replace($cacheData,$existCacheData);

        return $cacheData;
    }
}